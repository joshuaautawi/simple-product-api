const { Product, sequelize } = require("../models");

module.exports = {
  createProduct: async (req, res) => {
    const { id } = req.user;
    const { title, description, price, photo, availableStock } = req.body;
    try {
      const product = await Product.create({
        title,
        description,
        price,
        photo,
        availableStock,
        user_id: id,
      });
      if (!product) {
        return res
          .status(400)
          .json({ status: "failed", message: "Create product is failed !" });
      }
      return res.status(201).json({ status: "success", data: product });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed", message: "Error has occured ", error: e });
    }
  },
  showProduct: async (req, res) => {
    const { id } = req.user;
    try {
      const allProduct = await Product.findAll({
        where: {
          user_id: id,
        },
      });
      if (allProduct.length == 0) {
        return res
          .status(400)
          .json({ status: "failed", message: "No products are found  !" });
      }
      return res.status(200).json({ status: "success", data: allProduct });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed", message: "Error has occured ", error: e });
    }
  },
  updateProduct: async (req, res) => {
    const { user_id, productId, title, description, price, availableStock } =
      req.body;
    const { id } = req.user;
    if (user_id)
      return res
        .status(400)
        .json({ status: "failed", message: "User id cannot be changed !" });
    try {
      const product = await Product.update(
        {
          title,
          description,
          price,
          availableStock,
        },
        {
          where: {
            user_id: id,
            id: productId,
          },
        }
      );
      if (!product)
        return res
          .status(400)
          .json({ status: "failed", message: " update product failed !" });
      return res
        .status(200)
        .json({ status: "success", message: "Product has been update" });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed ", message: "error has occured ", error: e });
    }
  },
  deleteProduct: async (req, res) => {
    const { productId } = req.query;
    try {
      const product = await Product.destroy({ where: { id: productId } });
      if (!product) {
        return res
          .status(400)
          .json({ status: "failed", message: "Product Cannot been deleted !" });
      }
      return res
        .status(200)
        .json({ status: "failed", message: "Product deleted !" });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed", message: "Error has occured!", error: e });
    }
  },
  uploadPhotoProduct: async (req, res) => {
    const { productId } = req.body;
    try {
      const product = await Product.update(
        {
          photo: sequelize.fn(
            "array_append",
            sequelize.col("photo"),
            req.pp.secure_url
          ),
        },
        {
          where: {
            id: productId,
          },
        }
      );
      if (!product) {
        return res
          .status(400)
          .json({ status: "failed", message: " update product failed !" });
      }
      return res
        .status(200)
        .json({ status: "success", message: "photo has been upload !" });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed", message: "Error has occured ", error: e });
    }
  },
};
