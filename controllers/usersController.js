const { User } = require("../models");
const { encrypt, checkPassword } = require("../helpers/bycrpt");
const { getToken } = require("../helpers/jwt");

module.exports = {
  registerUser: async (req, res) => {
    const { fullname, email, password, profile_picture } = req.body;
    let token;
    try {
      const [user, created] = await User.findOrCreate({
        where: {
          email: req.body.email,
        },
        defaults: {
          fullname,
          email,
          profile_picture,
          password: encrypt(password),
        },
      });
      if (!created)
        return res
          .status(400)
          .json({ status: "failed", message: "this email has register !" });
      if (!user)
        return res
          .status(400)
          .json({ status: "failed", message: "User failed created !" });
      const payload = {
        id: user.id,
        fullname: user.fullname,
        email: user.email,
      };
      token = getToken(payload);
      return res
        .status(201)
        .json({ status: "success", token: token, data: payload });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed", message: "error has occured ", error: e });
    }
  },
  loginUser: async (req, res) => {
    const { email, password } = req.body;
    let token;
    try {
      const user = await User.findOne({
        where: {
          email,
        },
      });
      if (!checkPassword(password, user.password)) {
        return res
          .status(400)
          .json({ status: "failed", message: "password is not correct !" });
      }
      const payload = {
        id: user.id,
        fullname: user.fullname,
        email: user.email,
      };
      token = getToken(payload);
      return res.status(200).json({ status: "success", token: token });
    } catch (e) {
      return res
        .status(500)
        .json({ status: "failed", message: "error has occured ", error: e });
    }
  },
};
