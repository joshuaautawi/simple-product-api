const bcrypt = require("bcrypt");

module.exports = {
  encrypt: (password, saltRound = 10) => {
    return bcrypt.hashSync(password, saltRound);
  },

  checkPassword: (password, hash) => {
    return bcrypt.compareSync(password, hash);
  },
};
