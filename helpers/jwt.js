const { verify, sign } = require("jsonwebtoken");


module.exports = {
    checkToken : (token)=>{
        return verify(token,process.env.JWT_SECRET)
    },
    getToken : (payload)=>{
        return sign(payload,process.env.JWT_SECRET)
    }
}