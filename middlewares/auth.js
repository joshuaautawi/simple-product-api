const { checkToken } = require("../helpers/jwt");
const { User } = require("../models");

module.exports = {
  isLogin: async (req, res, next) => {
    let token;
    try {
      token = req.headers.authorization.split(" ")[1];

      if (!token)
        return res.status(401).json({
          status: "failed",
          message: "User is not authorized , please Login ",
          error: e,
        });
      const { email } = checkToken(token);
      req.user = await User.findOne({
        where: {
          email: email,
        },
      });
      if (req.user) next();
      else throw new Error();
    } catch (e) {
      return res.status(401).json({
        status: "failed",
        message: "User is not authorized , please Login ",
        error: e,
      });
    }
  },
};
