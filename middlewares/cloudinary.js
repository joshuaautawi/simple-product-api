const cloudinary = require("cloudinary").v2;
require("dotenv").config();

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_KEY,
  api_secret: process.env.CLOUD_SECRET,
});

module.exports = {
  upload: (req, res, next) => {
    const { base64 } = req.body;
    try {
      const file = "data:image/jpeg;base64," + base64;
      cloudinary.uploader
        .upload(file)
        .then((result) => {
          req.pp = result;
          next();
        })
        .catch((e) => {
          return res
            .status(400)
            .json({ status: "failed", error: e, message: "error has occured" });
        });
    } catch (e) {
      return res.status(400).json({
        status: "failed",
        error: e,
        message: "req.files.avatar is not found / Error has been occured",
      });
    }
  },
};
