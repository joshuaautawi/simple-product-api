const router = require("express").Router();
const userRoute = require("./user.route");
const productRoute = require("./product.route");
const { isLogin } = require("../middlewares/auth");

router.use("/users", userRoute);
router.use("/products", isLogin, productRoute);

module.exports = router;
