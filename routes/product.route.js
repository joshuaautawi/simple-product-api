const router = require("express").Router();
const {
  createProduct,
  deleteProduct,
  showProduct,
  updateProduct,
  uploadPhotoProduct,
} = require("../controllers/productsController");
const  { upload } = require('../middlewares/cloudinary')

router.get("/",showProduct);
router.post("/",createProduct);
router.patch("/",updateProduct);
router.patch("/photos",upload , uploadPhotoProduct);
router.delete("/",deleteProduct);

module.exports = router;
