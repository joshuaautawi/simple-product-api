const express = require("express");
const app = express();
const router = require("./routes/index");
require('dotenv').config()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", router);
app.get("/",(req,res)=>{
    res.status(200).json({status:"success", message : "Hellow World "})
})
app.listen(
  process.env.PORT,
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${process.env.PORT}`
  )
);
